//
//  Cosmetic.swift
//  Cosmetics
//
//  Created by Rauan Kussembayev on 11/5/16.
//  Copyright © 2016 kuzya. All rights reserved.
//

import Foundation


struct Cosmetic {
    var name : String
    var price : Int
    var image : String
    var id : String
    var description : String
}
