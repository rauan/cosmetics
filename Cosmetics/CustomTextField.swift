//
//  CustomTextField.swift
//  Cosmetics
//
//  Created by Rauan Kussembayev on 11/7/16.
//  Copyright © 2016 kuzya. All rights reserved.
//

import UIKit

@IBDesignable class CustomTextField: UITextField {
    @IBInspectable var placeHolderColor: UIColor? {
        get {
            return self.placeHolderColor
        }
        set {
            self.attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes:[NSForegroundColorAttributeName: newValue!])
        }
    }
    
    @IBInspectable var borderColor: UIColor? {
        get {
            return self.borderColor
        }
        set {
            self.layer.borderWidth = 1.0
            self.layer.cornerRadius = 5
            self.layer.borderColor = newValue?.cgColor
        }
    }
    
    func customizeTextFieldSuccess(isSucceed : Bool) {
        self.layer.borderWidth = 1.0
        self.layer.cornerRadius = 5
        if isSucceed {
            self.layer.borderColor = UIColor.green.cgColor
        } else {
            self.layer.borderColor = UIColor.white.cgColor
        }
    }
    
    func isValidEmail() -> Bool {
        
        let testStr = self.text
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        
        if emailTest.evaluate(with: testStr) {
            self.customizeTextFieldSuccess(isSucceed: true)
            return true
        }
        self.customizeTextFieldSuccess(isSucceed: false)
        return false
    }
    
    func isValidPasswrod() -> Bool {
        
        if (self.text?.characters.count)! > 5 && (self.text?.characters.count)! < 20 {
             self.customizeTextFieldSuccess(isSucceed : true)
            return true
        }
         self.customizeTextFieldSuccess(isSucceed : false)
        return false
    }
    
    func isValidLogin() -> Bool {
        if (self.text?.characters.count)! > 4 {
            self.customizeTextFieldSuccess(isSucceed : true)
            return true
        }
        self.customizeTextFieldSuccess(isSucceed : false)
        return false
    }
}
