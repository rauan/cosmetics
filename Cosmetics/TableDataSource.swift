//
//  File.swift
//  Cosmetics
//
//  Created by Rauan Kussembayev on 11/5/16.
//  Copyright © 2016 kuzya. All rights reserved.
//

import UIKit

public class TableDataSource: NSObject, UITableViewDataSource {
    
    var items: [Cosmetic]
    var cellIdentifier: String
    
    
    init(items: [Cosmetic]!, cellIdentifier: String!) {
        self.items = items
        self.cellIdentifier = cellIdentifier
        
        super.init()
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
 
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath as IndexPath) as! TableViewCell
        
        cell.nameLabel.text = items[indexPath.row].name
        cell.cosmeticImageView.image = UIImage(named: items[indexPath.row].image)
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        return cell

    }
    
    
    
}
