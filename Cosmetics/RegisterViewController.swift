//
//  RegisterViewController.swift
//  Cosmetics
//
//  Created by Rauan Kussembayev on 11/5/16.
//  Copyright © 2016 kuzya. All rights reserved.
//

import UIKit


class RegisterViewController: UIViewController {

    @IBOutlet weak var loginTextField: CustomTextField!
    @IBOutlet weak var emailTextField: CustomTextField!
    @IBOutlet weak var passwordTextField: CustomTextField!
    @IBOutlet weak var registerBtn: UIButton!
    
    
    
    var isLoginSucceed = false
    var isMailSucceed = false
    var isPasswordSucceed = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerBtn.isEnabled = false
        configureTextFields()
    }
    
    override func viewDidAppear(_ animated: Bool) {
         self.navigationController?.navigationBar.tintColor = UIColor.white
        UIApplication.shared.statusBarStyle = .lightContent
    }
    
    override func viewWillDisappear(_ animated: Bool) {
         self.navigationController?.navigationBar.tintColor = UIColor.black
        UIApplication.shared.statusBarStyle = UIStatusBarStyle.default
    }
    
   
    @IBAction func registerClicked(_ sender: UIButton) {
        
    }

}


// **************************************************************************************************
// MARK: - TextFields

extension RegisterViewController {
    
    func configureTextFields() {
        loginTextField.addTarget(self, action: #selector(RegisterViewController.textFieldDidChange(textField:)), for: UIControlEvents.editingChanged)
        emailTextField.addTarget(self, action: #selector(RegisterViewController.emailTextFieldDidChange), for: UIControlEvents.editingChanged)
        passwordTextField.addTarget(self, action: #selector(RegisterViewController.passwordTextFieldDidChange), for: UIControlEvents.editingChanged)
        
    }
    
    func passwordTextFieldDidChange() {
        
        if passwordTextField.isValidPasswrod() {
            isPasswordSucceed = true
        } else {
            isPasswordSucceed = false
        }
        updateRegisterBtnState()
    }
    
    
    func emailTextFieldDidChange() {
        
        if emailTextField.isValidEmail() {
            isMailSucceed = true
        } else {
            isMailSucceed = false
        }
        updateRegisterBtnState()
    }
    
    
    func textFieldDidChange(textField: UITextField) {
        
        if loginTextField.isValidLogin() {
            isLoginSucceed = true
        } else {
            isLoginSucceed = false
        }
        updateRegisterBtnState()
    }
    
}



// **************************************************************************************************
// MARK: - Conditions

extension RegisterViewController {
   
    func updateRegisterBtnState() {
        if isLoginSucceed && isMailSucceed && isPasswordSucceed {
            registerBtn.isEnabled = true
        } else {
            registerBtn.isEnabled = false
        }
    }
    
}
