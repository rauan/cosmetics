//
//  ViewController.swift
//  Cosmetics
//
//  Created by Rauan Kussembayev on 11/5/16.
//  Copyright © 2016 kuzya. All rights reserved.
//
import UIKit


class MainViewController: UIViewController {
    
    @IBOutlet weak var bannerView: BannerView!
    @IBOutlet weak var tablewView: UITableView!
    
    var dataSource:TableDataSource!
    var simulateRefresh = false
    let cosmeticInfoSegueIdentifier = "ShowCosmeticInfoSegue"
    var items = [Cosmetic]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        listPopularItems()
        configureTable()
        configureNavBar()
        configureFirstTime()
        items = listItems()
    }

    
    
}


// **************************************************************************************************
// MARK: - Navigation

extension MainViewController {
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if (segue.identifier ==  cosmeticInfoSegueIdentifier) {
            
            let cosmeticDetailViewController = segue.destination as! CosmeticDescriptionViewController
            let indexPath = self.tablewView.indexPathForSelectedRow!
            cosmeticDetailViewController.name = self.items[(indexPath as NSIndexPath).row].name
            cosmeticDetailViewController.image = self.items[(indexPath as NSIndexPath).row].image
            cosmeticDetailViewController.desc = self.items[(indexPath as NSIndexPath).row].description
            cosmeticDetailViewController.price = "\(self.items[(indexPath as NSIndexPath).row].price)"
            
        }
    }
    
}

// **************************************************************************************************
// MARK: - Configure

extension MainViewController {
    
    func configureNavBar() {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = UIColor.clear
        self.navigationController?.navigationBar.tintColor = UIColor.black
    }
    
    func configureTable() {
        dataSource = TableDataSource(items: listItems(), cellIdentifier: "Cell")
        tablewView.dataSource = dataSource
    }
    
    
    // MARK: - Get data
    
    func listItems()-> [Cosmetic]{
        var items = [Cosmetic]()
        CosmeticAPI.sharedInstance.listCosmetics { (result) in
            items = result
        }
        
        return items
    }
    
    func listPopularItems() {
        CosmeticAPI.sharedInstance.listPopularCosmetics { (result) in
            let items = result
            var arrayImages = [UIImage]()
            
            for item in items {
                if let image = UIImage(named: item.image) {
                    arrayImages.append(image)
                }
            }
            
            DispatchQueue.main.async {
                self.bannerView.createBanner(imagesArray: arrayImages)
                
            }
        }
        
    }
    
    func configureFirstTime() {
        let launchedBefore = UserDefaults.standard.bool(forKey: "launchedBefore")
        if launchedBefore  {
            
            print("Not first launch.")
        } else {
            print("First launch, setting UserDefault.")
            UserDefaults.standard.set(true, forKey: "launchedBefore")
            let firstPageViewController = self.storyboard?.instantiateViewController(withIdentifier: "FirstViewControllerIdentifier") as? FirstPageViewController
            self.navigationController?.pushViewController(firstPageViewController!, animated: true)
        }
        
        
    }
}
