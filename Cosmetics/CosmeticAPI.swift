//
//  CosmeticAPI.swift
//  Cosmetics
//
//  Created by Rauan Kussembayev on 11/5/16.
//  Copyright © 2016 kuzya. All rights reserved.
//

import Foundation

class CosmeticAPI {
    
    static let sharedInstance = CosmeticAPI()
    private var cosmeticsData = [Cosmetic]()
    private var cosmeticsPopularData = [Cosmetic]()
    
    func listCosmetics(onCompletion: @escaping ([Cosmetic]) -> Void) {
        cosmeticsData.removeAll()
        let cos1 = Cosmetic(name: "Pamada1", price: 300, image: "pr1", id: "1", description: "Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Nam liber te conscient to factor tum poen legum odioque civiuda.")
        let cos2 = Cosmetic(name: "Pamada2", price: 300, image: "pr2", id: "2", description: "Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Nam liber te conscient to factor tum poen legum odioque civiuda.")
        let cos3 = Cosmetic(name: "Pamada3", price: 300, image: "pr3", id: "3", description: "Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Nam liber te conscient to factor tum poen legum odioque civiuda.")
        let cos4 = Cosmetic(name: "Pamada4", price: 300, image: "pr4", id: "4", description: "Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Nam liber te conscient to factor tum poen legum odioque civiuda.")
        let cos5 = Cosmetic(name: "Pamada5", price: 300, image: "pr5", id: "5", description: "Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Nam liber te conscient to factor tum poen legum odioque civiuda.")
        
        self.cosmeticsData.append(cos1)
        self.cosmeticsData.append(cos2)
        self.cosmeticsData.append(cos3)
        self.cosmeticsData.append(cos4)
        self.cosmeticsData.append(cos5)
        
        onCompletion(self.cosmeticsData as [Cosmetic])
    }
    
    
    func listPopularCosmetics(onCompletion: @escaping ([Cosmetic]) -> Void) {
        cosmeticsPopularData.removeAll()
        let cos1 = Cosmetic(name: "Pamada1", price: 300, image: "bnr1", id: "3", description: "Some descrption")
        let cos2 = Cosmetic(name: "Pamada2", price: 300, image: "bnr2", id: "1", description: "Some descrption")
        let cos3 = Cosmetic(name: "Pamada3", price: 300, image: "bnr3", id: "4", description: "Some descrption")
        
        
        self.cosmeticsPopularData.append(cos1)
        self.cosmeticsPopularData.append(cos2)
        self.cosmeticsPopularData.append(cos3)
        
        onCompletion(self.cosmeticsPopularData as [Cosmetic])
    }
}
