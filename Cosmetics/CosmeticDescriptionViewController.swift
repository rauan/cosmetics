//
//  CosmeticDescriptionViewController.swift
//  Cosmetics
//
//  Created by Rauan Kussembayev on 11/5/16.
//  Copyright © 2016 kuzya. All rights reserved.
//

import UIKit

class CosmeticDescriptionViewController: UIViewController {

    var image = String()
    var price = String()
    var desc = String()
    var name = String()
    
    
    @IBOutlet weak var cosmeticImageView: UIImageView!
    @IBOutlet weak var cosmeticPrice: UILabel!
    @IBOutlet weak var cosmeticDescription: UITextView!
    @IBOutlet weak var cosmeticNameLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        cosmeticImageView.image = UIImage(named: image)
        cosmeticPrice.text = "\(price) T"
        cosmeticNameLabel.text = name
        cosmeticDescription.text = desc
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}
